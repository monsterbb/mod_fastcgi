#!/bin/bash

/usr/local/apache2/bin/apachectl stop
sleep 1
/usr/local/apache2/bin/apachectl stop
cp .libs/mod_fastcgi.so /usr/local/apache2/modules/mod_fastcgi.so
make install
/usr/local/apache2/bin/apachectl start
