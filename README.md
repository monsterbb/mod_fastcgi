### mod_fastcgi

This module is based on the original `mod_fastcgi` module. It compiles against `Apache` 2.4 and offers easier routing to
external fastcgi backend servers. Also, it offers rewriting the filename based fastcgi variables in case of the backend
is chrooted or has a different file path mapping. Tweaking is based on request notes (see mod_note here).


```

LoadModule fastcgi_module modules/mod_fastcgi.so
FastCgiExternalServer /fcgi_server_php -host 127.0.0.1:9000 -idle-timeout 120 -pass-header Authorization
FastCgiExternalServer /fcgi_server_mono -host 127.0.0.2:9000 -idle-timeout 120 -pass-header Authorization

LoadModule note_module modules/mod_note.so

AddHandler fastcgi/fcgi_server_php .php
AddHandler fastcgi/fcgi_server_mono .aspx .asmx .ashx .asax .ascx .soap .rem .axd .cs .config

<VirtualHost *>
  DocumentRoot /var/www/example1
  ServerName www.example.com/
  ServerAlias example.com

  SetNote /fcgi_server_php 127.0.0.1:9001
  SetNote /fcgi_server_mono mono-test:9000

  SetNote fastcgi_chroot /var/www
  SetNote fastcgi_prefix /web
  # this will transform the SCRIPT_FILENAME fcgi variable for http://example.com/index.php to /web/example1/index.php
  # while DOCUMENT_ROOT will become /web/example1

  AddHandler fastcgi/fcgi_server_mono .dll

</VirtualHost>

<VirtualHost *>
  DocumentRoot /var/www/example2/
  ServerName www.example.org
  ServerAlias example.org

  SetNote /fcgi_server_php 127.0.0.1:9002
  SetNote fastcgi_chroot /var/www/example2

  # this will transform the SCRIPT_FILENAME fcgi variable for http://example.org/index.php to /index.php
  # while DOCUMENT_ROOT will become /  (Note the trailing slash at the end of DocumentRoot)

</VirtualHost> 
```

It is also possible to override the idle timeout on per virtual host base:

```
  SetNote fastcgi_idle_timeout 900
```
